def main_feature():
    print("This is the main feature")


def some_feature():
    print("This is some feature")


def some_other_feature():
    print("This is some other feature")


def some_other_other_feature():
    print("This is some other other feature")


def new_feature_from_develop_2():
    print("This is a new feature from develop branch 2")


if __name__ == "__main__":
    main_feature()